import ChevronLeft from '../assets/back-button.png';
import DummyPhoto from '../assets/dummy.png';
import HeaderBG from '../assets/header-bg.png';
import QrIcon from '../assets/qr-icon.png';
import RecipetIcon from '../assets/recipet-icon.png';
import ShoppingBagIcon from '../assets/shoppingbag.png';
export default {
  ChevronLeft,
  DummyPhoto,
  HeaderBG,
  QrIcon,
  RecipetIcon,
  ShoppingBagIcon,
};
