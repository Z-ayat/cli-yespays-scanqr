export const colorButtons = [
  'rgb(131, 11, 20)',
  'rgb(0, 0, 0)',
  'rgb(0, 82, 211)',
  'rgb(252, 188, 4)',
];
export const sizeButtons = ['S', 'M', 'L'];
