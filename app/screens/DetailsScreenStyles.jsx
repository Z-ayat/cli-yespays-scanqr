import { StyleSheet } from 'react-native';
import { COLORS, FONTS, SIZES } from '../constants';

export const styles = StyleSheet.create({
  scrollContainer: {
    backgroundColor: COLORS.white,
    borderTopLeftRadius: SIZES.extraLarge,
    borderTopRightRadius: SIZES.extraLarge,
    flex: 1,
    marginTop: -50,
    paddingHorizontal: SIZES.extraLarge,
    paddingTop: SIZES.extraLarge,
  },
  detailsContainer: {
    marginVertical: SIZES.font,
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
  },
  productName: {
    fontFamily: FONTS.medium,
    color: COLORS.primary,
    fontSize: SIZES.large,
  },
  productPriceContainer: {
    alignItems: 'flex-end',
  },
  productPrice: {
    fontFamily: FONTS.semiBold,
    color: COLORS.price,
    marginBottom: SIZES.base,
  },
  productOldPrice: {
    color: COLORS.gray,
    textDecorationLine: 'line-through',
  },
  productDescription: { marginTop: SIZES.base, color: COLORS.gray },
  colorText: {
    color: COLORS.price,
    marginVertical: SIZES.small,
    fontFamily: FONTS.medium,
  },
  colorButtonContainer: {
    flexDirection: 'row',
    gap: SIZES.small,
  },
  sizeContainer: {},
  sizeText: {},
  sizeButtonContainer: {
    color: COLORS.price,
    marginVertical: SIZES.small,
    fontFamily: FONTS.medium,
  },
  colorContainer: {
    flexDirection: 'row',
    gap: SIZES.small,
  },
  promotionsContainer: {
    marginVertical: 30,
  },
});
