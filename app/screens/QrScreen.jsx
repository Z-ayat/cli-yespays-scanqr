import { View } from 'react-native';
import { Button } from 'react-native';
import { useDispatch } from 'react-redux';
import { setDone, setScanned } from '../store/qrScannerSlice';
const QrScreen = ({ route, navigation }) => {
  const { item } = route.params;
  const dispatch = useDispatch();
  return (
    <View
      style={{
        flex: 1,
        paddingTop: 200,
        alignItems: 'center',
        justifyContenr: 'center',
      }}>
      <Button
        title='scanned'
        onPress={() => {
          dispatch(setScanned(true));
          dispatch(setDone(item.barcode));
          navigation.replace('Details', { item });
        }}
      />
    </View>
  );
};
export default QrScreen;
