import { StyleSheet } from 'react-native';
import { ActivityIndicator } from 'react-native';
import { Text } from 'react-native';
import { View } from 'react-native';
import { Image } from 'react-native';
import Swiper from 'react-native-swiper';
import { COLORS, FONTS, SIZES } from '../constants';

const ImageSwiper = ({ product, loading }) => {
  return (
    <>
      {loading ? (
        <ActivityIndicator />
      ) : product?.images?.length > 0 ? (
        <Swiper
          style={styles.swiper}
          dotStyle={styles.swiperDot}
          activeDotStyle={styles.swiperActiveDot}>
          {product?.images?.slice(0, 4).map((img, idx) => (
            <Image
              key={++idx}
              resizeMode='contain'
              style={styles.productImage}
              src={`https://develop.yeshtery.com/files/${img?.url}`}
            />
          ))}
        </Swiper>
      ) : (
        <View style={styles.noImageContainer}>
          <Text style={styles.noImagesText}>Sorry No Images Available</Text>
        </View>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  swiper: { height: 241, marginBottom: 40 },
  swiperDot: { width: 10, height: 10, bottom: -20 },
  swiperActiveDot: { width: 25, bottom: -20 },
  productImage: {
    marginBottom: SIZES.extraLarge,
    width: '100%',
    height: 241,
    borderWidth: 1,
    borderColor: COLORS.gray,
    borderRadius: SIZES.extraLarge,
  },
  noImageContainer: {
    height: 241,
    marginBottom: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: COLORS.gray,
    borderRadius: SIZES.extraLarge,
  },
  noImagesText: {
    fontFamily: FONTS.bold,
    fontSize: SIZES.extraLarge,
    color: COLORS.header,
  },
});
export default ImageSwiper;
