import React from 'react';
import { Image } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { View, Text } from 'react-native';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { useSelector } from 'react-redux';
import { FONTS, SIZES } from '../constants';
export const ButtonWithTextAndIcon = ({
  onPressHandler,
  iconImg,
  text,
  scanDone,
}) => {
  return (
    <TouchableWithoutFeedback
      onPress={onPressHandler}
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        gap: 10,
        paddingHorizontal: 20,
        paddingVertical: 10,
        backgroundColor: '#FAFAFA',
        borderRadius: 6,
      }}
      pointerEvents='none'>
      {!scanDone && <Image source={iconImg} />}
      <Text
        style={{
          fontSize: SIZES.medium,
          color: scanDone ? 'green' : '#1F54CF',
          fontFamily: FONTS.medium,
        }}>
        {text}
      </Text>
    </TouchableWithoutFeedback>
  );
};

export const ColorsButton = ({ color }) => {
  return (
    <View
      style={{
        width: 24,
        height: 24,
        backgroundColor: color,
        borderRadius: 1000,
      }}></View>
  );
};

export const SizeButton = ({ size }) => {
  return (
    <View
      style={{
        width: 70,
        height: 32,
        backgroundColor: 'transparent',
        borderWidth: 0.5,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 40,
      }}>
      <Text>{size}</Text>
    </View>
  );
};
