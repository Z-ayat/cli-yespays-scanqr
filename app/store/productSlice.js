import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';

const initialState = {
  records: [],
  loading: false,
  error: null,
  product: [],
};

export const fetchProducts = createAsyncThunk(
  'products/fetchProducts',
  async (_, thunkApi) => {
    const { rejectWithValue } = thunkApi;
    try {
      const fetch = await axios
        .get('https://api-dev.yeshtery.com/v1/yeshtery/products')
        .then((res) => {
          return res.data.products;
        });
      const data = await fetch;
      return data;
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);
export const fetchProduct = createAsyncThunk(
  'products/fetchProduct',
  async (id, thunkApi) => {
    const { rejectWithValue } = thunkApi;
    try {
      const fetch = await axios
        .get(
          `https://api-dev.yeshtery.com/v1/yeshtery/product?product_id=${id}`
        )
        .then((res) => {
          return res.data;
        });
      const data = await fetch;
      return data;
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

const productSlice = createSlice({
  name: 'products',
  initialState,
  reducers: {
    cleanRecords(state) {
      state.product = [];
    },
  },
  extraReducers: (builder) => {
    // fetch all products
    builder.addCase(fetchProducts.pending, (state) => {
      state.loading = true;
      state.error = null;
    });
    builder.addCase(fetchProducts.fulfilled, (state, action) => {
      state.loading = false;
      state.records = action.payload;
    });
    builder.addCase(fetchProducts.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
    });
    // fetch single product
    builder.addCase(fetchProduct.pending, (state) => {
      state.loading = true;
      state.error = null;
    });
    builder.addCase(fetchProduct.fulfilled, (state, action) => {
      state.loading = false;
      state.product = action.payload;
    });
    builder.addCase(fetchProduct.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
    });
  },
});
export const { cleanRecords } = productSlice.actions;

export default productSlice.reducer;
