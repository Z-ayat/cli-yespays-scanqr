import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  isScanned: false,
  scanned: [],
};

const qrScannerSlice = createSlice({
  name: 'qrScanner',
  initialState,
  reducers: {
    setScanned(state, action) {
      state.isScanned = action.payload;
    },
    setDone(state, action) {
      if (state.scanned.includes(action.payload))
        return console.log('Already Scanned');
      state.scanned.push(action.payload);
    },
  },
});
export const { setScanned, setDone } = qrScannerSlice.actions;

export default qrScannerSlice.reducer;
